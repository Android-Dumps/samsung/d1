#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_SHIPPING_API_LEVEL := 28

PRODUCT_CHARACTERISTICS := phone

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Rootdir
PRODUCT_PACKAGES += \
    gps.sh \
    install-recovery.sh \

PRODUCT_PACKAGES += \
    fstab.exynos9825 \
    init.recovery.exynos9825.rc \
    init.recovery.samsung.rc \

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/samsung/d1/d1-vendor.mk)
