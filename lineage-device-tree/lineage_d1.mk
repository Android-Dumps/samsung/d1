#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from d1 device
$(call inherit-product, device/samsung/d1/device.mk)

PRODUCT_DEVICE := d1
PRODUCT_NAME := lineage_d1
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-N970F
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="d1xx-user 12 SP1A.210812.016 N970FXXS7HVD5 release-keys"

BUILD_FINGERPRINT := samsung/d1xx/d1:12/SP1A.210812.016/N970FXXS7HVD5:user/release-keys
